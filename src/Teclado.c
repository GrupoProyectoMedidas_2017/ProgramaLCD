/*
 * Teclado.c
 *
 *  Created on: 16 de dic. de 2017
 *      Author: Rodrigo
 */

/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Blinky using FreeRTOS.
 *
 *
 * NOTE: It's interesting to check behavior differences between standard and
 * tickless mode. Set @ref configUSE_TICKLESS_IDLE to 1, increment a counter
 * in @ref vApplicationTickHook and print the counter value every second
 * inside a task. In standard mode the counter will have a value around 1000.
 * In tickless mode, it will be around 25.
 *
 */

/** \addtogroup rtos_blink FreeRTOS blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "Teclado.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void configTeclado()
{
//	Chip_GPIO_WriteDirBit(LPC_GPIO_PORT, BOTON_ON_OFF, 	false);
//	Chip_GPIO_WriteDirBit(LPC_GPIO_PORT, BOTON_HOLD,    false);
//	Chip_GPIO_WriteDirBit(LPC_GPIO_PORT, BOTON_CONV,    false);

	/* LEDs EDU-CIAA-NXP */
	Chip_SCU_PinMux(BOTON_SCU_ON_OFF, MD_PUP|MD_EZI, FUNC0);
	Chip_SCU_PinMux(BOTON_SCU_CONV, MD_PUP|MD_EZI, FUNC0);
	Chip_SCU_PinMux(BOTON_SCU_HOLD, MD_PUP|MD_EZI, FUNC0);

	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, BOTON_GPIO_PIN_ON_OFF);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, BOTON_GPIO_PIN_CONV);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, BOTON_GPIO_PIN_HOLD);
}

uint8_t antirrebote(uint8_t port, uint8_t pin)
{
	//Completar esta funcion
	if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, port, pin)==BOTON_ON)
	{
		vTaskDelay(DELAY_REBOTE_MS / portTICK_RATE_MS);
		if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, port, pin)==BOTON_ON)
		{
			return BOTON_PRESIONADO;
		}
		else
		{
			return BOTON_NO_PRESIONADO;
		}
	}
	else
	{
		return BOTON_NO_PRESIONADO;
	}
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
