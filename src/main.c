/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Blinky using FreeRTOS.
 *
 *
 * NOTE: It's interesting to check behavior differences between standard and
 * tickless mode. Set @ref configUSE_TICKLESS_IDLE to 1, increment a counter
 * in @ref vApplicationTickHook and print the counter value every second
 * inside a task. In standard mode the counter will have a value around 1000.
 * In tickless mode, it will be around 25.
 *
 */

/** \addtogroup rtos_blink FreeRTOS blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "main.h"

#include "board.h"

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"

#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "LCD.h"
#include "Teclado.h"
#include "ADS1115.h"

#include "string.h"

/*==================[macros and definitions]=================================*/


//Defines calculos
#define BAR_A_KPA 100
#define BAR_A_PSI 14.5038
#define PSI_A_KGCM 0.070306957829636

//Defines electrovalvula
//T_COL2
#define BOTON_SCU_ELECTROVALVULA	    	7, 5//2, 4
#define BOTON_GPIO_PIN_ELECTROVALVULA	    3, 13//5, 4

//T_COL1
//#define BOTON_SCU_ELECTROVALVULA	    	7, 4//2, 4
//#define BOTON_GPIO_PIN_ELECTROVALVULA	    3, 12//5, 4

//#define BOTON_SCU_ELECTROVALVULA	    	2, 11//2, 4
//#define BOTON_GPIO_PIN_ELECTROVALVULA	    	1, 11//2, 4
//#define BOTON_GPIO_BIT_ELECTROVALVULA	    5, (1<<4)

//Defines ADC
//I2C0
//Creo que se configuran automaticamente, pero igual se quemo...
//I2C1
#define ADC_SCU_SDA	    		2, 3//RS232_TXD
#define ADC_GPIO_PIN_SDA	    5, 3
#define ADC_GPIO_BIT_SDA	    5, (1<<3)

#define ADC_SCU_SCL	    		2, 4//RS232_RXD
#define ADC_GPIO_PIN_SCL	    5, 4
#define ADC_GPIO_BIT_SCL	    5, (1<<4)

//Defines MUX
#define MUX_SCU_D				6, 1			//GPIO0 -- D
#define MUX_SCU_C				6, 5			//GPIO2 -- C
#define MUX_SCU_A				6, 8			//GPIO4 -- A //func 4
#define MUX_SCU_B				6, 10			//GPIO6 -- B

#define MUX_GPIO_PIN_D			3, 0
#define MUX_GPIO_PIN_C			3, 4
#define MUX_GPIO_PIN_A			5, 16
#define MUX_GPIO_PIN_B			3, 6
/*==================[internal data declaration]==============================*/

typedef enum unidades {PSI=0, KPA, BAR, CUENTAS, TENSION, KGCM} UNIDAD;
typedef enum Estados  {OFF=0, ON, HOLD, CALIBRACION0, CALIBRACION1, CALIBRACION2} ESTADOS_MAQ;

static uint16_t valor_adc;
static UNIDAD unidad_valor;
ESTADOS_MAQ estado_menu;
uint8_t estado_electrovalvula = 0;
uint16_t C0 = 4012;
uint16_t C1 = 5315;
uint16_t C2 = 29090;


/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

static void configElectrovalvula(void);
static void configADC(void);
static void configMUX(void);

static float Convertir_Presion(uint16_t, uint8_t);
static void	Imprimir_Presion(float, uint8_t);
static void	Imprimir_Unidad(uint8_t);

static float Algoritmo_Presion(uint16_t cuenta);
static float Algoritmo_Tension (uint16_t cuenta);
static float Algoritmo_Presion2(uint16_t cuenta);
static float Algoritmo_Presion3(uint16_t cuenta);
static float Algoritmo_Presion4 (uint16_t cuenta);


/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void)
{
	SystemCoreClockUpdate();

	Board_Init();

	Board_LED_Set(0, false);
}

static void configElectrovalvula(void)
{
	//Electrovalvula
	Chip_SCU_PinMux(BOTON_SCU_ELECTROVALVULA,MD_PUP|MD_EZI,FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, BOTON_GPIO_PIN_ELECTROVALVULA);
}

static void configMUX(void)
{
	//Multiplexor
	Chip_SCU_PinMux(MUX_SCU_A, MD_PUP|MD_EZI, FUNC4);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MUX_GPIO_PIN_A);

	Chip_SCU_PinMux(MUX_SCU_B, MD_PUP|MD_EZI, FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MUX_GPIO_PIN_B);

	Chip_SCU_PinMux(MUX_SCU_C, MD_PUP|MD_EZI, FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MUX_GPIO_PIN_C);

	Chip_SCU_PinMux(MUX_SCU_D, MD_PUP|MD_EZI, FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, MUX_GPIO_PIN_D);

	//Selecciono el Canal 5
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, MUX_GPIO_PIN_A, 1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, MUX_GPIO_PIN_B, 0);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, MUX_GPIO_PIN_C, 1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, MUX_GPIO_PIN_D, 0);
}

static void configADC(void)
{
	//Configuracion de los pines del ADC
	Chip_SCU_PinMux(ADC_SCU_SCL, MD_PUP|MD_EZI,FUNC4);
	Chip_GPIO_SetDir(LPC_GPIO_PORT, ADC_GPIO_BIT_SCL, false);

	Chip_SCU_PinMux(ADC_SCU_SDA, MD_PUP|MD_EZI,FUNC4);
	Chip_GPIO_SetDir(LPC_GPIO_PORT, ADC_GPIO_BIT_SDA, false);
}

static void	Imprimir_Presion(float aux, uint8_t unidad_valor)
{
	char str[10];
	aux = Convertir_Presion(aux, unidad_valor);

	if(aux<0)
		aux=0;

	if(unidad_valor == CUENTAS)
		sprintf(str, "%.0f", aux);
	else if(unidad_valor == PSI)
		sprintf(str, "%.2f", aux);
	else if(unidad_valor == KPA)
		sprintf(str, "%.1f", aux);
	else if(unidad_valor == KGCM)
		sprintf(str, "%.4f", aux);
	else
		sprintf(str, "%.3f", aux);

	Print_LCD(str);
	Print_LCD(" ");
	Imprimir_Unidad(unidad_valor);
}


static void	Imprimir_Unidad(uint8_t unidad_valor)
{
		if (unidad_valor==KPA)
			Print_LCD("KPA");

		if (unidad_valor==PSI)
			Print_LCD("PSI");

		if (unidad_valor==BAR)
			Print_LCD("BAR");

		if (unidad_valor==CUENTAS)
			Print_LCD("Cuentas");

		if (unidad_valor==TENSION)
			Print_LCD("Volt");

		if (unidad_valor==KGCM)
			Print_LCD("KG/CM");
}


static float Convertir_Presion(uint16_t cuenta, uint8_t unidad_valor)
{
	if (unidad_valor==KPA)
		return (Algoritmo_Presion(cuenta)*BAR_A_KPA);

	if (unidad_valor==PSI)
//		return (Algoritmo_Presion(cuenta)*BAR_A_PSI);
		return Algoritmo_Presion4(cuenta);

	if (unidad_valor==BAR)
		return (Algoritmo_Presion(cuenta));

	if (unidad_valor==TENSION)
		return Algoritmo_Tension(cuenta);

	if (unidad_valor==CUENTAS)
		return cuenta;

	if (unidad_valor==KGCM)
		return Algoritmo_Presion4(cuenta)*PSI_A_KGCM;

	else
		return (Algoritmo_Presion(cuenta));
}


static float Algoritmo_Presion (uint16_t cuenta)
{
// PRESION EN BAR
	float Pmin = 0;     //PRESION MINIMA
	float Pmax = 10;  // PRESION MAXIMA 10 BARES
	float Vsupply = 5;  // TENSION ALIMENTACION SENSOR
	float Vmedida = Algoritmo_Tension(cuenta);
	return ( ((Vmedida-0.1*Vsupply)*(Pmax-Pmin)/(0.8*Vsupply)) - Pmin );


//	float presion;
//	float Vmedida = Algoritmo_Tension(cuenta);
//
//	if (cuenta < 12000)
//	{
//		presion = (Vmedida*46.15385) - 25.84616;
//	}
//	else if ( (12000 >= cuenta) && (cuenta < 15000) )
//	{
//		presion = (Vmedida*38.46154) - 18.07693;
//	}
//	else if ( (15000 >= cuenta) && (cuenta <= 18000) )
//	{
//		presion = (Vmedida*36.58536) - 14.87804;
//	}
//	else if (cuenta > 18000)
//	{
//
//		presion = (Vmedida*43.47826) - 33.91304;
//	}
//	return presion;
}

static float Algoritmo_Tension (uint16_t cuenta)
{
	float valor_lsb = 125;  //valor FSR 4.096V

	return (cuenta*valor_lsb/1000000);
}

static float Algoritmo_Presion2 (uint16_t cuenta)
{
// PRESION EN PSI
	return ( (cuenta - 4000)/213.3333328 );
}

static float Algoritmo_Presion3 (uint16_t cuenta)
{
//A y B hallados en base a la tabla que hicimos
	float A = 0.00462671;
	float B = -14.59095689;
// PRESION EN PSI
	return ( A*cuenta + B );
}

static float Algoritmo_Presion4 (uint16_t cuenta)
{
	uint16_t P0 = 0;
	uint16_t P1 = 10;
	uint16_t P2 = 120;
//	uint16_t C0 = ----;
//	uint16_t C1 = 5315;
//	uint16_t C2 = 29090;

	if (cuenta<5315)
	{
		float A = ((float)(P1-P0)/(float)(C1-C0));
		float B = P1-A*C1;
// PRESION EN PSI
		return ( A*cuenta + B );
	}
	else
	{
		float A = ((float)(P2-P1)/(float)(C2-C1));
		float B = P2-A*C2;
// PRESION EN PSI
		return ( A*cuenta + B );

	}
}

static void tarea_ADC(void * d)
{
	uint16_t config = 0;
	uint16_t dato = 65535;

	ADS1115_Init();
	config = ADS1115_GeneralCall();
	ADS1115_SetConfigRegister(MODE_SAMU2);

	vTaskDelay(1000 / portTICK_RATE_MS);

	config = ADS1115_GetConfigRegister();

	while (1)
	{
		ADS1115_ReadConversion(10, &dato);
		valor_adc = dato;
		vTaskDelay(100 / portTICK_RATE_MS);
	}
}

//static void tarea_LCD(void * b)
//{
//	Init_LCD();
//
//	Write_LCD_Nibble(CURSOR_START, LCD_INS);
//	cursor=0;
//	fila=0;
//
//	Print_LCD("Iniciando");
//	vTaskDelay(1000 / portTICK_RATE_MS);
//	Print_LCD(".");
//	vTaskDelay(500 / portTICK_RATE_MS);
//	Print_LCD(".");
//	vTaskDelay(500 / portTICK_RATE_MS);
//	Print_LCD(".");
//	vTaskDelay(500 / portTICK_RATE_MS);
//	Clear_LCD();
//
//	while (1){
//	    Saltar_Fila_LCD(0);
//	    Print_LCD("Presion: ");
//	    Imprimir_Presion(Convertir_Presion(valor_adc));
//
//
//	//	Write_LCD_Nibble(CURSOR_START, LCD_INS);
//	}
//}

static void setElectrovalvula(uint8_t on)
{
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, BOTON_GPIO_PIN_ELECTROVALVULA, on);
	Board_LED_Set(5, !on);
}

static uint8_t getElectrovalvula(void)
{
	return Chip_GPIO_GetPinState(LPC_GPIO_PORT, BOTON_GPIO_PIN_ELECTROVALVULA);
}

static void leerTeclado()
{
	if(antirrebote(BOTON_GPIO_PIN_ON_OFF)==BOTON_PRESIONADO)
	{
		if(estado_menu == ON)
		{
			estado_menu = OFF;
		}
		else if(estado_menu == OFF)
		{
			estado_menu = ON;
			Print_LCD("Bienvenido");
			vTaskDelay(500/ portTICK_RATE_MS);
			Print_LCD(".");
			vTaskDelay(200/ portTICK_RATE_MS);
			Print_LCD(".");
			vTaskDelay(200/ portTICK_RATE_MS);
			Print_LCD(".");
			vTaskDelay(500/ portTICK_RATE_MS);
			Clear_LCD();
		}
	}

	if(antirrebote(BOTON_GPIO_PIN_HOLD)==BOTON_PRESIONADO)
	{
		if (estado_menu == HOLD)
		{
			estado_menu = ON;
		}
		else if (estado_menu == ON)
		{
			estado_menu = HOLD;
		}
	}

	if(antirrebote(BOTON_GPIO_PIN_CONV)==BOTON_PRESIONADO)
	{
		if (estado_electrovalvula == 1)
		{
			estado_electrovalvula = 0;
			setElectrovalvula(0);
		}
		else if (estado_electrovalvula == 0)
		{
			estado_electrovalvula = 1;
			setElectrovalvula(1);
		}

//		if (unidad_valor == PSI)
//		{
//			unidad_valor = KPA;
//		}
//
//		else if (unidad_valor == KPA)
//		{
//			unidad_valor = BAR;
//		}
//
//		else if (unidad_valor == BAR)
//		{
//			unidad_valor = PSI;
//		}
	}

	if(antirrebote(BOTON_GPIO_PIN_OTRO)==BOTON_PRESIONADO)
	{
		if (estado_menu == ON)
		{
			estado_menu = CALIBRACION0;
			setElectrovalvula(1);

			Clear_LCD();
		}
		else if (estado_menu == CALIBRACION0)
		{
//			setElectrovalvula(0);
//			vTaskDelay(1000 / portTICK_RATE_MS);

			C0 = valor_adc+5;//Hacemos esta trampita para que nunca muestre mas de cero cuando este en cero

//			setElectrovalvula(1);

			estado_menu = CALIBRACION1;
			Clear_LCD();
		}
		else if (estado_menu == CALIBRACION1)
		{
//			setElectrovalvula(0);
//			vTaskDelay(1000 / portTICK_RATE_MS);

			C1 = valor_adc;

//			setElectrovalvula(1);

			estado_menu = CALIBRACION2;
			Clear_LCD();
		}
		else
		{
//			setElectrovalvula(0);
//			vTaskDelay(1000 / portTICK_RATE_MS);

			C2 = valor_adc;

//			setElectrovalvula(1);

			estado_menu = ON;
			Clear_LCD();
		}
	}
}

static void tarea_Teclado(void * a)
{
	Init_LCD();

	float valor_hold;
	estado_menu = OFF;
//	unidad_valor = KPA;

	while (1)
	{
		leerTeclado();

		switch(estado_menu)
		{
			case OFF:
						Clear_LCD();
						Board_LED_Set(3, false);
						vTaskDelay(500 / portTICK_RATE_MS);
						break;

			case ON:
						valor_hold = valor_adc;
						Board_LED_Set(3, true);

						Saltar_Fila_LCD(0);
						Print_LCD("Medicion:           ");

						Saltar_Fila_LCD(1);
//						unidad_valor = KPA;
//						Imprimir_Presion(valor_adc, unidad_valor);
//						unidad_valor = BAR;
//						Imprimir_Presion(valor_adc, unidad_valor);
//						unidad_valor = TENSION;
//						Imprimir_Presion(valor_adc, unidad_valor);
						unidad_valor = CUENTAS;
						Imprimir_Presion(valor_adc, unidad_valor);

						Saltar_Fila_LCD(2);
						unidad_valor = KGCM;
						Imprimir_Presion(valor_adc, unidad_valor);
//
						Saltar_Fila_LCD(3);
						unidad_valor = PSI;
						Imprimir_Presion(valor_adc, unidad_valor);
						break;

			case HOLD:
						Saltar_Fila_LCD(0);
						Print_LCD("Medicion:     >Hold<");

						Saltar_Fila_LCD(1);
//						unidad_valor = KPA;
//						Imprimir_Presion(valor_adc, unidad_valor);
						unidad_valor = CUENTAS;
						Imprimir_Presion(valor_hold, unidad_valor);

//						Saltar_Fila_LCD(2);
//						unidad_valor = BAR;
//						Imprimir_Presion(valor_hold, unidad_valor);
//
//						Saltar_Fila_LCD(3);
//						unidad_valor = PSI;
//						Imprimir_Presion(valor_hold, unidad_valor);
						break;

			case CALIBRACION0:
						Saltar_Fila_LCD(0);
						Print_LCD("Presiona CALIBRAR");
						Saltar_Fila_LCD(1);
						Print_LCD("cuando el manometro");
						Saltar_Fila_LCD(2);
						Print_LCD("indique 0 PSI");

						break;

			case CALIBRACION1:
						Saltar_Fila_LCD(0);
						Print_LCD("Presiona CALIBRAR");
						Saltar_Fila_LCD(1);
						Print_LCD("cuando el manometro");
						Saltar_Fila_LCD(2);
						Print_LCD("indique 10 PSI");
						break;

			case CALIBRACION2:
						Saltar_Fila_LCD(0);
						Print_LCD("Presiona CALIBRAR");
						Saltar_Fila_LCD(1);
						Print_LCD("cuando el manometro");
						Saltar_Fila_LCD(2);
						Print_LCD("indique 120 PSI");
						break;

			default:
						estado_menu = OFF;
						break;
		}
	}
}


static void task_LED(void * a)
{
	while (1)
	{
		Board_LED_Toggle(0);
		vTaskDelay(500 / portTICK_RATE_MS);
	}
}


/*==================[external functions definition]==========================*/

int main(void)
{
	initHardware();

	configLCD();
	configADC();
	configTeclado();
	configElectrovalvula();
	configMUX();

	xTaskCreate(task_LED, (const char *)"task_LED", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
//	xTaskCreate(tarea_LCD, (const char *)"tarea_LCD", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(tarea_ADC, (const char *)"tarea_ADC", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(tarea_Teclado, (const char *)"tarea_Teclado", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);

	vTaskStartScheduler();

	while (1) {
	}
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/


