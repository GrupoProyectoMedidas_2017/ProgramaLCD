/*
 * Teclado.c
 *
 *  Created on: 16 de dic. de 2017
 *      Author: Rodrigo
 */

/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Blinky using FreeRTOS.
 *
 *
 * NOTE: It's interesting to check behavior differences between standard and
 * tickless mode. Set @ref configUSE_TICKLESS_IDLE to 1, increment a counter
 * in @ref vApplicationTickHook and print the counter value every second
 * inside a task. In standard mode the counter will have a value around 1000.
 * In tickless mode, it will be around 25.
 *
 */

/** \addtogroup rtos_blink FreeRTOS blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "LCD.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void configLCD()
{
   /* LEDs EDU-CIAA-NXP */
   Chip_SCU_PinMux(LCD_SCU_D4,MD_PUP|MD_EZI,FUNC0);
   Chip_SCU_PinMux(LCD_SCU_D5,MD_PUP|MD_EZI,FUNC0);
   Chip_SCU_PinMux(LCD_SCU_D6,MD_PUP|MD_EZI,FUNC0);
   Chip_SCU_PinMux(LCD_SCU_D7,MD_PUP|MD_EZI,FUNC4);
   Chip_SCU_PinMux(LCD_SCU_EN,MD_PUP|MD_EZI,FUNC4);
   Chip_SCU_PinMux(LCD_SCU_RS,MD_PUP|MD_EZI,FUNC4);

   Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LCD_GPIO_PIN_D4);
   Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LCD_GPIO_PIN_D5);
   Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LCD_GPIO_PIN_D6);
   Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LCD_GPIO_PIN_D7);
   Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LCD_GPIO_PIN_EN);
   Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LCD_GPIO_PIN_RS);
}

void Write_LCD(uint32_t data, bool type)
{
	//La funcion WritePortBit es equivalente a SetPin en LPCOpen, antes estaba dentro de la funcion SetPin
	//Pero lo saque de estar como funcion aparte para simplificar el codigo.
	//El parametro LPC_GPIO_PORT va por defecto siempre que se use esta funcion. Es lo unico que SetPin me ahorraba
	Chip_GPIO_WritePortBit(LPC_GPIO_PORT, LCD_GPIO_PIN_RS, type);

	Chip_GPIO_WritePortBit(LPC_GPIO_PORT, LCD_GPIO_PIN_EN, true);

	Chip_GPIO_WritePortBit(LPC_GPIO_PORT, LCD_GPIO_PIN_D4, data &1);
	Chip_GPIO_WritePortBit(LPC_GPIO_PORT, LCD_GPIO_PIN_D5,(data >>1)&1);
	Chip_GPIO_WritePortBit(LPC_GPIO_PORT, LCD_GPIO_PIN_D6,(data >>2)&1);
	Chip_GPIO_WritePortBit(LPC_GPIO_PORT, LCD_GPIO_PIN_D7,(data >>3)&1);

	Chip_GPIO_WritePortBit(LPC_GPIO_PORT, LCD_GPIO_PIN_EN,false);
}

//se la debe llamar con el define LCD_INS(Command) o LCD_DATA(Data) en type si es una instruccion o dato respectivamente.
void Write_LCD_Nibble(uint32_t data, bool type)
{
	Write_LCD((data>>4), type);
	vTaskDelay(DELAY_LCD_MS / portTICK_RATE_MS);
	Write_LCD(data, type);
	vTaskDelay(DELAY_LCD_MS / portTICK_RATE_MS);
}

void Clear_LCD()
{
	Write_LCD_Nibble(CLR_SCREEN, LCD_INS);
	cursor=0;
	fila=0 /*FILA1*/;
}

void Print_LCD(char *data)
{
	uint8_t i;

	for (i=0; i<strlen(data); i++)
	{
		comprobarcursor();
		Write_LCD_Nibble(data[i], LCD_DATA);
		cursor++;
	}
}


//Hago una nueva version de comprobar cursor, igual a la que estaba arriba pero elimine,  pero esta vez
//expandida para el display de 4 lineas
void comprobarcursor()
{
	if (cursor > COLUMNAS_MAX-1)
	{
		cursor=0;
		if(fila==0)
		{
			lcd_gotoxy(1,0);
			fila = 1;
		}

		else if(fila==1)
		{
			lcd_gotoxy(2,0);//Para 4 lineas hay que usar esta.
			fila = 2;
		}

		else if(fila==2)
		{
			lcd_gotoxy(3,0);
			fila = 3;
		}

		else if(fila==3)
		{
			lcd_gotoxy(0,0);//Para 4 lineas hay que usar esta.
			fila = 0;
		}
	}
}


//Hice esta version de comprobarcursor(), pero no funciona del todo bien.
//La idea seria eliminar la variable fila y de a poco cursor.
//void comprobarcursor()
//{
//	//Chequear si esta parte quedo bien! y mejorarla porque no anda totalmente bien.
//	if ((0 < cursor) && (cursor < COLUMNAS_MAX)){
//		fila =0;
////		lcd_gotoxy(0,cursor-1);
//	}
//
//	if ((COLUMNAS_MAX < cursor) && (cursor < COLUMNAS_MAX*2)){
//		fila = 1;
//		lcd_gotoxy(1,0);
//	}
//
//	if ((COLUMNAS_MAX*2 < cursor) && (cursor < COLUMNAS_MAX*3)){
//		fila =2;
//		lcd_gotoxy(2,0);
//	}
//
//	if ((COLUMNAS_MAX*3 < cursor) && (cursor < COLUMNAS_MAX*4)){
//		fila =3;
//		lcd_gotoxy(3,0);
//	}
//
//	if (COLUMNAS_MAX*4 < cursor){
//		cursor=0;
//		fila =0;
//		lcd_gotoxy(0,0);
//	}
//}

void lcd_gotoxy(uint8_t y, uint8_t x) {
    if ( y==0 )
    	Write_LCD_Nibble((1<<LCD_DDRAM)+LCD_START_LINE1+x, LCD_INS);
    else if ( y==1)
    	Write_LCD_Nibble((1<<LCD_DDRAM)+LCD_START_LINE2+x, LCD_INS);
    else if ( y==2)
    	Write_LCD_Nibble((1<<LCD_DDRAM)+LCD_START_LINE3+x, LCD_INS);
    else /* y==3 */
    	Write_LCD_Nibble((1<<LCD_DDRAM)+LCD_START_LINE4+x, LCD_INS);
}

//Siguiendo el esquema de comprobarcursor, hago una version extendida de Saltar_Fila_LCD
void Saltar_Fila_LCD(uint32_t fila)
{
	if (fila==0){
		lcd_gotoxy(0,0);
		cursor = 0;
		fila=0;
	}

	if (fila==1){
		lcd_gotoxy(1,0);
		cursor = 0;
		fila=1;
	}

	if (fila==2){
		lcd_gotoxy(2,0);
		cursor = 0;
		fila=2;
	}

	if (fila==3){
		lcd_gotoxy(3,0);
		cursor = 0;
		fila=3;
	}
}

void Init_LCD(void)
{
	cursor = 0;
	fila = 0;

//Esto estaba dentro de un estado, pero lo saco para simplificar y adecuar a lo que es Freertos
	vTaskDelay(DELAY_LCD_INIT_MS / portTICK_RATE_MS);
	Write_LCD((LCD_4BITS>>4),LCD_INS);
	vTaskDelay(DELAY_LCD_MS / portTICK_RATE_MS);

	Write_LCD_Nibble(LCD_2LINEAS, LCD_INS);
	Write_LCD_Nibble(LCD_ON, LCD_INS);
	Write_LCD_Nibble(CURSOR_START, LCD_INS);
	Write_LCD_Nibble(CLR_SCREEN, LCD_INS);
}


/** @} doxygen end group definition */

/*==================[end of file]============================================*/
