/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _TECLADO_H_
#define _TECLADO_H_

/** \addtogroup rtos_blink FreeRTOS blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

///////////////Otros Defines
#define DELAY_REBOTE_MS 10

////Defines botones
#define BOTON_SCU_ON_OFF	    	4, 3//Tecla 3 - Tec_F3
//#define BOTON_SCU_OTRO				4, 0//Tecla 0 - Tec_F0
#define BOTON_SCU_CONV          	4, 2//Tecla 2 - Tec_F2
#define BOTON_SCU_HOLD          	4, 1 //Tecla 1 - Tec_F1

#define BOTON_GPIO_PIN_ON_OFF	    2, 3
//#define BOTON_GPIO_PIN_OTRO			2, 0
#define BOTON_GPIO_PIN_CONV         2, 2
#define BOTON_GPIO_PIN_HOLD         2, 1

//Defines teclas EDU-CIAA
//#define BOTON_SCU_ON_OFF	    	1, 0//Tecla 3 - Tec_F3
#define BOTON_SCU_OTRO				1, 1//Tecla 0 - Tec_F0
//#define BOTON_SCU_CONV          	1, 2//Tecla 2 - Tec_F2
//#define BOTON_SCU_HOLD          	1, 6 //Tecla 1 - Tec_F1
//
//#define BOTON_GPIO_PIN_ON_OFF	    0, 4
#define BOTON_GPIO_PIN_OTRO			0, 8
//#define BOTON_GPIO_PIN_CONV         0, 9
//#define BOTON_GPIO_PIN_HOLD         1, 9

//#define BOTON_GPIO_BIT_ON_OFF		2, (1<<3)	//abajo - izq
//#define BOTON_GPIO_BIT_OTRO		2, (1<<0)	////abajo - der
//#define BOTON_GPIO_BIT_CONV		2, (1<<2)	//arriba - izq
//#define BOTON_GPIO_BIT_HOLD		2, (1<<1)	//// arriba - der

#define BOTON_ON 			0
#define BOTON_OFF			1

#define BOTON_PRESIONADO	1
#define BOTON_NO_PRESIONADO	0

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @brief config teclado function
 * @return none
 */
void configTeclado(void);

/** @brief leer teclado function
 * @param port este es el puerto del boton
 * @param pin este es el pin del boton
 * @return none
 */
uint8_t antirrebote(uint8_t port, uint8_t pin);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _TECLADO_H_ */
