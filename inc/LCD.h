/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _LCD_H_
#define _LCD_H_

/** \addtogroup rtos_blink FreeRTOS blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "string.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

//#define halfPeriodMS 200

/////////////Defines LCD
#define DELAY_LCD_MS 5	//para este 100mSeg es una buena velocidad
#define DELAY_LCD_INIT_MS 500

//estos son para LPC, los comento porque debo agregar SCU y GPIO de LCD
//#define LCD_D4 2,4
//#define LCD_D5 2,5
//#define LCD_D6 2,6
//#define LCD_D7 2,7
//#define LCD_EN 2,8
//#define LCD_RS 2,10

#define LCD_SCU_D4 4, 4
#define LCD_SCU_D5 4, 5
#define LCD_SCU_D6 4, 6
#define LCD_SCU_D7 4, 10
#define LCD_SCU_RS 4, 8
#define LCD_SCU_EN 4, 9

#define LCD_GPIO_PIN_D4 2, 4
#define LCD_GPIO_PIN_D5 2, 5
#define LCD_GPIO_PIN_D6 2, 6
#define LCD_GPIO_PIN_D7 5, 14
#define LCD_GPIO_PIN_RS 5, 12
#define LCD_GPIO_PIN_EN 5, 13

//#define LCD_GPIO_BIT_D4 2, (1<<4)
//#define LCD_GPIO_BIT_D5 2, (1<<5)
//#define LCD_GPIO_BIT_D6 2, (1<<6)
//#define LCD_GPIO_BIT_D7 5, (1<<14)
//#define LCD_GPIO_BIT_RS 5, (1<<12)
//#define LCD_GPIO_BIT_EN 5, (1<<13)

#define LCD_4BITS			0x20		// 0b0010 0000
#define LCD_2LINEAS			0x28 		// 0b0010 1000
#define LCD_ON				0x0C		// 0b0000 1100
#define CLR_SCREEN			0x01		// 0b0000 0001
#define CURSOR_START		0x02		// 0b0000 0010
#define LCD_CURSOR_MOVE		LCD_RIGHT
#define LCD_LEFT			0x04		// 0b0000 0100
#define LCD_RIGHT			0x06		// 0b0000 0110
#define LCD_DATA			true
#define LCD_INS			    false

#define MAX_PRINT_LCD       50

#define LINEA1				0x80		//128  	0b1000 0000
#define LINEA2				0xC0		//192	0b1100 0000
#define COLUMNAS_MAX		20			//16
//#define LINEAS				4			//No se por que esto esta definido, no se usa
//#define FILA1				0
//#define FILA2				1

#define LCD_DDRAM			7			//  	0b0000 0111

#define LCD_START_LINE1 0x0
#define LCD_START_LINE2 0x040
#define LCD_START_LINE3 0x014
#define LCD_START_LINE4 0x054

#define MUESTREO_LCD  20

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

static uint32_t cursor;
static uint32_t fila;

/*==================[external functions declaration]=========================*/

 //static void configlcd();

 /** @brief delay function
  * @param port PUERTO
  * @param pin  PIN
  * @param state true = 1 / false = 0
  */
 //void Set_pin(uint32_t port, uint32_t pin, bool state);

 void Saltar_Fila_LCD(uint32_t);

 /** @brief delay function
  * @param data dato a escribir
  * @param type true = dato / false = instruccion
  */
 void Write_LCD(uint32_t data,bool type);

 /** ------------------@brief delay function
  * @param data Instruccion o dato a escribir
  * @param type Aca debe decir si se escribe dato o instruccion con los defines LCD_INS y LCD_DATA
  */
 void Write_LCD_Nibble(uint32_t data, bool type);

 /** @brief delay function
  * @param data dato a escribir
  */
 //static void Write_LCD_Data(uint32_t data);

 void Clear_LCD();

 void Print_LCD(char *data);

 void comprobarcursor();

 void lcd_gotoxy(uint8_t y, uint8_t x);

 /** @brief delay function
  * @param data instruccion a escribir
   */
 //static void Write_LCD_Command(uint32_t data);

 void configLCD(void);
 void Init_LCD(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _TECLADO_H_ */
